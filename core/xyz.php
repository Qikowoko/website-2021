<?php if ($root=="") exit;

echo '<div class="containercomic">'."\n";

# Include the language selection menu
include($file_root.'core/mod-menu-lang.php');

# Array of all pages
$allpages = glob(''.$sources.'/new-ep[0-9][0-9]*/low-res/'.$lang.''.$credits.'E[0-9][0-9]P[0-9][0-9].*');
if (empty($allpages)) {
  # - fallback to hardcoded English if no translation are available for $lang
  $allpages = glob(''.$sources.'/new-ep[0-9][0-9]*/low-res/en'.$credits.'E[0-9][0-9]P[0-9][0-9].*');
  $fallbackmode = 1;
} else {
  $fallbackmode = 0;
}
sort($allpages);

echo '  <div style="clear:both"></div>'."\n";
echo '  </br>'."\n";

echo '    <div class="notification" style="text-align: center;"><b> XYZ area: spoiler alert! Please, do not reshare this page</b>:<br/> This episode is still in development and is not meant to be ready for public. It\'s published here only to help proofreader and contributors of Pepper&Carrot. If you want to help and give a feedback, <a href="https://framagit.org/peppercarrot/webcomics/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=future%20episode">join our latest thread on Framagit here.</div>';

$newepisodepathinfo = pathinfo($allpages[0]);
$newepisodepath = $newepisodepathinfo['dirname'];
$newepisodepath = str_replace('/low-res', '', $newepisodepath);

# If a file "secret" exists at root of the folder, do not list the pages.
$secrettoken = ''.$newepisodepath.'/secret';
if (!file_exists($secrettoken)) {
  # Display header
  if (!empty($allpages)) {
    $titlepage = ''.$root.'/'.$allpages[0].'';
    if (file_exists($allpages[0])) {
      echo '  <div class="panel" align="center">'."\n";
      echo '    <img class="comicpage" src="'.$titlepage.'" alt="'._("Header").'" /></a>'."\n";
      echo '  </div>'."\n";
      echo ''."\n";
      # Remove the page from the array
      unset($allpages[0]);
    }



    # Display the comic pages
    foreach ($allpages as $key => $page) {
      $pagepath = ''.$root.'/'.$page.'';
      if (file_exists($page)) {
        echo '  <article class="panel">'."\n";

        $comic_alt = $header_title.', '._("Page").' '.$key.'';
        $title_alt = ''._("Page").' '.$key.'';

        # Gif: special rule to upscale them
        if (strpos($pagepath, 'gif') == true) {
          echo '    <img class="comicpage" style="max-width:'.$gif_width.';padding-bottom: '.$gif_padding.';" width="92%" src="'.$pagepath.'" alt="'.$comic_alt.' title="'.$title_alt.'" "/>'."\n";
        } else {
        # Regular comic page
          echo '    <img class="comicpage" src="'.$pagepath.'" alt="'.$comic_alt.'" title="'.$title_alt.'" />'."\n";
        }
        echo '  </article>'."\n";
      }
    }
  }
  echo ''."\n";
}

# Container end
echo '</div>'."\n";
echo ''."\n";
echo '  </br>'."\n";
echo '  </br>'."\n";
echo '  </br>'."\n";
echo ''."\n";
?>
